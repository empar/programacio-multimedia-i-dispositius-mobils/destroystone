﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InterfaceAwake : MonoBehaviour
{
    private AudioSource audio;
    // Start is called before the first frame update
    void Start()
    {
        GameManager.currentNumberDestroyedStones = 0;
        GameManager.currentNumberStone = 0;
        GameManager.currentNumberDestroyedAstroBomb = 0;
        audio = GetComponent<AudioSource>();
        audio.Play();

    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void Click()
    {
        Application.LoadLevel("stoneGame");
    }
}
