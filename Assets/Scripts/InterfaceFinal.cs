﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InterfaceFinal : MonoBehaviour
{
    public Text texStone;

    public Text textDestroyed;

    public Text textAstroDestroyed;
    public Text total;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        texStone.text = "Numero de Objetos: " + GameManager.currentNumberStone;
        textDestroyed.text = "Objetos Destruidas: " + GameManager.currentNumberDestroyedStones;
        textAstroDestroyed.text = "Puntos restados: " + GameManager.currentNumberDestroyedAstroBomb;
        total.text = "Puntuación final: " + (GameManager.currentNumberDestroyedStones+GameManager.currentNumberDestroyedAstroBomb);

    }

    public void Click()
    {
        Application.LoadLevel("Awake");
    }
}
