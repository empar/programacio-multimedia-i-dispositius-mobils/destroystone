﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InterfaceStone : MonoBehaviour {
    public Text texStone;

    public Text textDestroyed;

    public Text textAstroDestroyed;
    private AudioSource audio;
    
    // Start is called before the first frame update
    void Start()
    {
        audio = GetComponent<AudioSource>();
            audio.Play();
        
    }

    // Update is called once per frame
    void Update()
    {
        texStone.text = "Numero de Objetos: " + GameManager.currentNumberStone;
        textDestroyed.text = "Objetos Destruidas: " + GameManager.currentNumberDestroyedStones;
        textAstroDestroyed.text = "Puntos restados: " + GameManager.currentNumberDestroyedAstroBomb;
      
        

    }
}
